import React from 'react';
import './App.css';
import Reporter from './Reporter'; // Make sure the path is correct based on your project structure

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* ... existing content ... */}
      </header>
      <Reporter name="Antero Mertaranta">Löikö mörkö sisään</Reporter>
      <Reporter name="Kevin McGran">I know it's a rough time now, but did you at least enjoy playing in the tournament?</Reporter>
    </div>
  );
}

export default App;