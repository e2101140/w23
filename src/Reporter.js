import React from 'react';

const Reporter = ({ name, children }) => {
  return (
    <div>
      <p>{name}: {children}</p>
    </div>
  );
};

export default Reporter;